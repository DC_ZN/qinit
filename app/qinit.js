const Express = require('express');
const app = new Express();

const BodyParser = require('body-parser');
app.use(BodyParser.json());

const port = 3001;
const router = Express.Router();

router.post('/', (req, res) => {
  let tag = "undefined_username";
  try {
    // TODO: move the token to config file
    if (req.body.token !== "lah8xai4ongaigoonga7esohyaeba8ruh8uurailugishu4eev") {
      console.log(tag, `Access denied, username: ${req.body.username}`);
      throw new Error("");
    }

    const username = req.body.username;
    if (username === undefined || typeof username !== 'string') {
      throw new Error("username is undefined or not a string");
    }
    tag = username;

    const quest_name = req.body.quest_name;
    if (quest_name === undefined) {
      throw new Error("quest_name is undefined");
    }

    const quest_inputs = req.body.quest_inputs;
    if (quest_inputs === undefined || typeof quest_inputs !== 'object') {
      throw new Error("quest_inputs must be an object");
    }
    console.log(tag, 'Start processing quest init request');

    let quest_outputs = null;
    // TODO: read quest table and respective handlers
    if (quest_name === "encrypted_disk") {
      quest_outputs = handle_quest_encrypted_disk(quest_inputs);
    } else if (quest_name === "backdoored_apk") {
      quest_outputs = handle_quest_backdoored_apk(quest_inputs);
    }

    if (quest_outputs !== null) {
      res.status(200).json(quest_outputs).send();
    } else {
      const err_msg = "quest not found";
      console.log(tag, err_msg);
      res.status(503).json({"error":err_msg}).send();
    }

  } catch (err) {
    console.log(tag, err);
    res.status(503).json({"error":err.message}).send();
  }
});

// "abc.onion" -> "noino.cba." - needed for tor browser bookmarks for some reason
function get_rev_host(host) {
  return host.split("").reverse().join("") + ".";
}

const uuidv4 = require('uuid/v4');
const { execSync } = require("child_process");
const tmp = require("tmp");

function handle_quest_encrypted_disk(quest_inputs) {
  const password = quest_inputs.password;
  if (password === undefined) {
    throw new Error("Encryption password is undefined");
  }

  const onion = quest_inputs.onion;
  if (onion === undefined) {
    throw new Error("Onion address is undefined");
  }

  console.log(`Creating encrypted disk with password: ${quest_inputs.password}`);
  // __dirname = absolute path to <deploy>/services/qinit/app
  const wd = __dirname + "/../../../quests/encrypted_disk_quest/";
  const output_archive_path = __dirname + "/../../../quests/generated/";

  const disk_size_m = 62; // generated disk size
  const partitions = "1,2,3,4"; // list of partition numbers
  const enc_partition_num = 1;
  const unenc_partition_num = 3;
  const disk_name = uuidv4();
  const disk_path = "/tmp/";

  let tmpdir = null;

  try {
    execSync(`./bin/grapemaker.sh make -s ${disk_size_m} -f ${partitions} ${disk_path}${disk_name}`, { "cwd":wd });
    execSync(`./bin/grapemaker.sh cp ${wd}/res/unencrypted_files/. ${disk_path}${disk_name}:/${unenc_partition_num}/`, { "cwd":wd });
    execSync(`./bin/grapemaker.sh encrypt --partition=${enc_partition_num} --password=${password} ${disk_path}${disk_name}`, { "cwd":wd });

    tmpdir = tmp.dirSync();
    execSync(`cp -R ${wd}/res/encrypted_files/. ${tmpdir.name}`);

//    execSync(`sed -Ei 's/cipollino_link/${onion}/g' ${tmpdir.name}/.tb/tor-browser/Browser/TorBrowser/Data/Browser/profile.default/bookmarks.html`);
    const rev_host = get_rev_host(onion);
    const bookmarks_file = tmpdir.name + "/.tb/Browser/TorBrowser/Data/Browser/profile.default/places.sqlite";
    execSync(`sqlite3 ${bookmarks_file} 'UPDATE moz_places SET url = "http://${onion}/", rev_host = "${rev_host}" WHERE id = 16;'`);
    execSync(`sqlite3 ${bookmarks_file} 'UPDATE moz_hosts SET host = "${onion}" WHERE id = 5;'`);

    execSync(`./bin/grapemaker.sh cp --encrypted --encrypted-password=${password} --encrypted-pim=1 ${tmpdir.name}/. ${disk_path}${disk_name}:/${enc_partition_num}/`, { "cwd":wd });
    execSync(`./bin/grapemaker.sh rm ${disk_path}${disk_name} ${enc_partition_num}`, { "cwd":wd });

    execSync(`tar -C ${disk_path} -czf ${output_archive_path}${disk_name}.tar.gz ${disk_name}`);
  } finally {
    execSync(`rm ${disk_path}${disk_name}`);
    if (tmpdir !== null) {
      console.log("cleanup tmp dir")
      execSync(`rm -r ${tmpdir.name}`);
    }
  }

  return {"quest_outputs":{"filename":`${disk_name}.tar.gz`}};
}

function btoa(mstr) {
    return Buffer.from(mstr).toString('base64')
}

function atob(mstr) {
    return Buffer.from(mstr, 'base64').toString()
}

function tel_encode(mstr, code=117) {
    var bytes = [];
    var res_str = '';
    for (var i=0; i<mstr.length; i++) {
        res_str += String.fromCharCode(mstr.charCodeAt(i)^code);
    }
    return btoa(res_str);
}

function tel_decode(mbase, code=117) {
    var bytes = atob(mbase);
    var res_str = '';
    for (var i=0; i<bytes.length; i++) {
        res_str += String.fromCharCode(bytes.charCodeAt(i)^code);
    }
    return res_str;
}

function handle_quest_backdoored_apk(quest_inputs) {
  const onion = quest_inputs.onion;
  if (onion === undefined) {
    throw new Error("Onion address for backdoored_apk is undefined");
  }

  console.log(`Creating backdoored_apk with onion: ${quest_inputs.onion}`);
  // __dirname = absolute path to <deploy>/services/qinit/app
  const wd = "/root/tel-yo-gram/"
  const apk_name = uuidv4();
  const output_path = __dirname + "/../../../quests/generated/" + apk_name;
  let input_onion = tel_encode(quest_inputs.onion);
  console.log(`Normal onion: ${quest_inputs.onion}, encoded url: ${input_onion}`);

  execSync(`./telyogram-builder.sh ${input_onion} ${output_path}`, { "cwd": wd });

  return {"quest_outputs":{"filename":`${apk_name}`}};
}

app.use('/v1/init', router);

app.get('/', (req, res) => res.json({ message: "DO NOT HACK pls -- internal quest infrastructure" }));
// Usage:
// Initialize specified quest for the user:
//   POST /v1/init ContentType: application/json {"token":"security_token", "username":"user1", "quest_name":"myquest", "quest_inputs":{<quest_specific_data>}}
// Responses:
//   200, {"quest_outputs":{<quest_specific_data}} on success.
//   503, {"error":"Error msg"} on error.

app.listen(port, () => console.log('Qinit listening on port ' + port));

// curl -X GET -H "Content-Type: application/json" "http://localhost:3000/"
// curl -X POST -H "Content-Type: application/json" http://localhost:3000/v1/init -d '{"token":"secret", "username":"abc12", "quest_name":"myqyest", "quest_inputs":{"password":"pass1", "onion":"blabla.onion"}}'
